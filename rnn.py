import tensorflow as tf
from tensorflow import keras
from tensorflow import set_random_seed

from tensorflow.keras import backend as K

import numpy as np
from numpy.random import seed

from matplotlib import pyplot as plt

import ai_metrics as metrics

seed(1)
set_random_seed(2)

np.set_printoptions(suppress=True)

BATCH_SIZE = 32

class KerasRNN:

  def __init__(self, processor,NEURONS,LR,TYPE,HOURS,EPOCH,BATCH,MULTIPLIER):

    self.processor = processor

    self.hours = HOURS
    self.checkpoint = TYPE + "_" + str(HOURS) + "_hours_checkpoint.keras"
    self.batch  = BATCH
    self.neurons = NEURONS
    self.learning = LR
    self.epochs = EPOCH
    self.multiplier = MULTIPLIER

    self.sequence_length = self.hours * self.multiplier

    self.type = TYPE

    self.processor.generate_validation_data()

  # https://jmlb.github.io/ml/2017/03/20/CoeffDetermination_CustomMetric4Keras/
  def regression_coefficient(self, y_true, y_test):
    SS_res =  K.sum(K.square( y_true-y_test )) 
    SS_tot = K.sum(K.square( y_true - K.mean(y_true) ) ) 
    return ( 1 - SS_res/(SS_tot + K.epsilon()) )
  
  def build_model(self):
    self.model = keras.Sequential()

    if self.type == 'lstm':
      self.build_lstm_layers(self.neurons)
    else:
      self.build_gru_layers(self.neurons)

    self.model.add(keras.layers.Dense(self.processor.n_outputs))

    self.model.summary()
    
    self.model.compile(
      optimizer=keras.optimizers.SGD(lr=self.learning),
      loss=tf.losses.mean_squared_error,
      metrics=[
        keras.metrics.MAE,
        metrics.pearson_r,
        metrics.R_squared,
        metrics.fac2
      ]
    )

  def build_lstm_layers(self, neurons):
    self.model.add(keras.layers.LSTM(
      units=neurons[0],
      dropout=0.1,
      recurrent_dropout=0.1,
      return_sequences= True,
      input_shape=(None, self.processor.n_inputs)
    ))
    
    for key in range(len(neurons)-1):
      self.model.add(keras.layers.LSTM(
        units=neurons[key+1],
        recurrent_dropout=0.1,
        return_sequences=True
      ))
  
  def build_gru_layers(self, neurons):
    self.model.add(keras.layers.GRU(
      units=neurons[0],
      return_sequences= True,
      activation="tanh",
      input_shape=(None, self.processor.n_inputs)
    ))
    
    for key in range(len(neurons)-1):
      self.model.add(keras.layers.GRU(
        units=neurons[key+1],
        activation="tanh",
        return_sequences=True
      ))
    
  
  def train(self):
    checkpoint = keras.callbacks.ModelCheckpoint(
      filepath=self.checkpoint,
      monitor="val_loss",
      verbose=1,
      save_weights_only=True,
      save_best_only=True
    )
    early_stopping = keras.callbacks.EarlyStopping(
      monitor="val_loss",
      patience=10,
      min_delta=1e-4,
      mode='auto',
      verbose=1
    )
    tensorboard = keras.callbacks.TensorBoard(log_dir="./logs/", histogram_freq=0, write_graph=False)
    reduce_lr = keras.callbacks.ReduceLROnPlateau(
        monitor='val_loss',
        factor=0.98,
        min_lr=1e-6,
        patience=0,
        verbose=1
    )
    
    callbacks = [
      checkpoint, early_stopping, tensorboard, reduce_lr
    ]

    history = self.model.fit_generator(
      generator=self.processor.batch_generator(batch_size=self.batch, sequence_length=self.sequence_length),
      epochs=self.epochs,
      steps_per_epoch=self.processor.x_train_scaled.shape[0]/self.batch,
      validation_data=self.processor.validation_data,
      callbacks=callbacks
    )

    plt.plot(history.history['loss'], label='Training Loss')
    plt.plot(history.history['val_loss'], label='Validation Loss')
    plt.title("Training and Validation losses")
    plt.legend()
    plt.show()

  def evaluate(self):
    result = self.model.evaluate(x=np.expand_dims(self.processor.x_test_scaled, axis=0),
      y=np.expand_dims(self.processor.y_test, axis=0))

    for res, metric in zip(result, self.model.metrics_names):
        print("{0}: {1:.3e}".format(metric, res))

  def predict(self):
    x_scaled = self.processor.x_test_scaled
    y = self.processor.y_test
    
    x_scaled = self.processor.expand_dims(x_scaled)

    y_test = self.model.predict(x_scaled)
    y_test = y_test[0]
    #y_test_rescaled = self.processor.y_scaler.inverse_transform(y_test[0])

    # For each output-signal.
    for signal in range(len(self.processor.target_names)):
      # Get the output-signal predicted by the model.
      signal_pred = y_test[:, signal]
        
      # Get the true output-signal from the data-set.
      signal_true = y[:, signal]
        
      # Plot and compare the two signals.
      plt.plot(signal_true, label='Measured')
      plt.plot(signal_pred, label='Prediction')
        
      # Plot labels etc.
      plt.ylabel(self.processor.target_names[signal])
      plt.legend()
      plt.show()