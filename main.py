import argparse
import pandas as pd
from rnn import KerasRNN
from data import DataProcessor

NEURONS = [72]
LR = 1e-3
TYPE = 'lstm'
HOURS = 1
EPOCH = 100
BATCH = 20
MULTIPLIER = 7

if TYPE != 'gru' and TYPE != 'lstm':
  raise ValueError("Invalid type")
    
shift_days = 1

shift_steps = shift_days * HOURS

filename = "dataset.csv"
dataset = pd.read_csv(filename, header=0, index_col=0)

#dataset.drop(['day', 'month', 'year', 'hour'], axis=1)

processor = DataProcessor(dataset)
processor.shift(shift_steps)

x_data, y_data = processor.to_numpy_arrays(shift_steps)
processor.build_dataset(x_data, y_data)

#processor.scale()
#reframed = processor.series_to_supervised(hours, 1)

rnn = KerasRNN(processor,NEURONS,LR,TYPE,HOURS,EPOCH,BATCH,MULTIPLIER)
    
rnn.build_model()
    
try:
  rnn.model.load_weights(rnn.checkpoint)
        
  rnn.evaluate()
  rnn.predict()
except Exception as error:
  print("Error trying to load checkpoint.")
  print(error)
  rnn.train()
  rnn.evaluate()
  rnn.predict()