import numpy as np
from sklearn.preprocessing import StandardScaler

TARGET_NAMES = ['v_anemo']

NUM_TOTAL = 744
NUM_TRAIN = 550
NUM_TEST = NUM_TOTAL - NUM_TRAIN

TESTING_SPLIT = 0.3
VALIDATION_SPLIT = 0.2

NUM_TEST = int(NUM_TRAIN*VALIDATION_SPLIT)

class DataProcessor:

  def __init__(self, dataset):
    self.raw_dataset = dataset
    self.x_scaler = StandardScaler()
    self.y_scaler = StandardScaler()

    self.target_names = TARGET_NAMES
    self.validation_split = VALIDATION_SPLIT

  def shift(self, shift_steps):
    self.dataset_targets = self.raw_dataset[TARGET_NAMES].shift(-shift_steps)

  def to_numpy_arrays(self, shift_steps):
    x_data = self.raw_dataset.values[0:-shift_steps]
    y_data = self.dataset_targets.values[:-shift_steps]
  
    return x_data, y_data

  def rescale(self, data, x=True):
    return self.x_scaler.inverse_transform(data[0]) if x else self.y_scaler.inverse_transform(data[0])

  def build_dataset(self, x_data, y_data):

    self.n_inputs = x_data.shape[1]
    self.n_outputs = y_data.shape[1]

    x_train = x_data[0:(NUM_TRAIN-NUM_TEST)]
    self.y_train = y_data[0:(NUM_TRAIN-NUM_TEST)]

    x_val = x_data[(NUM_TRAIN-NUM_TEST):NUM_TRAIN]
    self.y_val = y_data[(NUM_TRAIN-NUM_TEST):NUM_TRAIN]

    x_test = x_data[NUM_TRAIN:NUM_TOTAL]
    self.y_test = y_data[NUM_TRAIN:NUM_TOTAL]

    self.x_train_scaled = self.x_scaler.fit_transform(x_train)
    #self.y_train_scaled = self.y_scaler.fit_transform(self.y_train)

    self.x_val_scaled = self.x_scaler.transform(x_val)
    #self.y_val_scaled = self.y_scaler.transform(self.y_val)
    
    self.x_test_scaled = self.x_scaler.transform(x_test)
    #self.y_test_scaled = self.y_scaler.transform(self.y_test)

  def batch_generator(self, batch_size, sequence_length):

    while True:
      x_shape = (batch_size, sequence_length, self.n_inputs)
      y_shape = (batch_size, sequence_length, self.n_outputs)

      x_batch = np.zeros(shape=x_shape, dtype=np.float32)
      y_batch = np.zeros(shape=y_shape, dtype=np.float32)

      for i in range(batch_size):
        idx = np.random.randint(NUM_TRAIN - NUM_TEST - sequence_length)

        x_batch[i] = self.x_train_scaled[idx:idx+sequence_length]
        y_batch[i] = self.y_train[idx:idx+sequence_length]

      yield(x_batch, y_batch)

  def generate_validation_data(self):
    self.validation_data = (self.expand_dims(self.x_val_scaled),
      self.expand_dims(self.y_val))

  def expand_dims(self, data):
    return np.expand_dims(data, axis=0)
